# 一些效果示例

[![](https://github.styleci.io/repos/127072458/shield?branch=master)](https://github.styleci.io/analyses/z9aN5w#)

1. [404页面](https://liluoao.github.io/html-demo/404.html)
2. [彩色折线](https://liluoao.github.io/html-demo/polyline.html)
3. [particlesJS加泡泡效果](https://liluoao.github.io/html-demo/particlesJS-demo.html)
4. [live2D猫随鼠标摇头](https://liluoao.github.io/html-demo/live2d.html)
